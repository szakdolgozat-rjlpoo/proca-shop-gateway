package xyz.nergal.proca.gateway.data;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
public class PriceChange {

    private Long productId;

    private Price price;
}
