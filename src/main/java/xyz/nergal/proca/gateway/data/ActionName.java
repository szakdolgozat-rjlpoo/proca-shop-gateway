package xyz.nergal.proca.gateway.data;

public class ActionName {

    private ActionName() {
    }

    public static class Product {

        private Product() {
        }

        public static final String UNSUBSCRIBE = "product:unsubscribe";
        public static final String LIST = "product:list";
        public static final String DETAILS = "product:details";
    }

    public static class Price {

        private Price() {
        }

        public static final String CHANGE = "price:change";
    }

    public static class Internal {

        private Internal() {
        }

        public static final String ERROR = "internal:error";

        public static final String PING = "internal:ping";
        public static final String PONG = "internal:pong";
    }
}
