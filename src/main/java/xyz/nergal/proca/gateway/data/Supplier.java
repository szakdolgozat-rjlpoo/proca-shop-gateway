package xyz.nergal.proca.gateway.data;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
public class Supplier {

    private String name;
}
