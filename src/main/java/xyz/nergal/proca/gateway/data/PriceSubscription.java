package xyz.nergal.proca.gateway.data;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
public class PriceSubscription {

    private Product product;

    private boolean detailed;
}
