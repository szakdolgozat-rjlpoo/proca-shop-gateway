package xyz.nergal.proca.gateway.data;

import lombok.*;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
public class Product {

    private Long id;

    private String name;

    private String description;

    private String img;

    private Set<String> categories;

    private List<Price> prices;
}
