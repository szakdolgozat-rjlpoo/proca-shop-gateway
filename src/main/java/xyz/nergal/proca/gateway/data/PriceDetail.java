package xyz.nergal.proca.gateway.data;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
public class PriceDetail {

    private String supplier;

    private Long stock;
}
