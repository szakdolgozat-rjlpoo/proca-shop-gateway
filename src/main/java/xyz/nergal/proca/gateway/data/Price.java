package xyz.nergal.proca.gateway.data;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
public class Price {

    private Long id;

    private String currencyCode;

    private BigDecimal value;

    private LocalDateTime updatedAt;

    private PriceDetail detail;
}
