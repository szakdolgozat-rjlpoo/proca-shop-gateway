package xyz.nergal.proca.gateway.handler.payload.input;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.domain.Pageable;
import xyz.nergal.proca.gateway.handler.payload.InputPayload;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ProductListInputPayload extends InputPayload<ProductListInputPayload.ProductListData> {

    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class ProductListData {

        private Pageable pageable;
    }
}
