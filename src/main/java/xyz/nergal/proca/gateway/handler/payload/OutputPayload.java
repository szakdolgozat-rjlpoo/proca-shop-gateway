package xyz.nergal.proca.gateway.handler.payload;

import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class OutputPayload<T> {

    private String action;

    private T data;
}
