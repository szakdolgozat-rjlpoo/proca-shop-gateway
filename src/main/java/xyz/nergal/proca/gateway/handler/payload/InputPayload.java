package xyz.nergal.proca.gateway.handler.payload;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.nergal.proca.gateway.handler.payload.input.EmptyInputPayload;
import xyz.nergal.proca.gateway.handler.payload.input.ProductDetailsInputPayload;
import xyz.nergal.proca.gateway.handler.payload.input.ProductListInputPayload;

import javax.annotation.Nullable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "action",
        visible = true,
        defaultImpl = EmptyInputPayload.class
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ProductListInputPayload.class, name = "product:list"),
        @JsonSubTypes.Type(value = ProductDetailsInputPayload.class, name = "product:details")
})
public abstract class InputPayload<T> {

    private String action;

    @Nullable
    private T data;
}
