package xyz.nergal.proca.gateway.handler.payload.input;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.nergal.proca.gateway.handler.payload.InputPayload;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class EmptyInputPayload extends InputPayload<Object> {
}
