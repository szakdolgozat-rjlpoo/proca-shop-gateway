package xyz.nergal.proca.gateway.handler;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import xyz.nergal.proca.gateway.data.*;
import xyz.nergal.proca.gateway.event.PriceChangedEvent;
import xyz.nergal.proca.gateway.event.PriceChangedEventEmitter;
import xyz.nergal.proca.gateway.handler.payload.InputPayload;
import xyz.nergal.proca.gateway.handler.payload.OutputPayload;
import xyz.nergal.proca.gateway.handler.payload.input.ProductDetailsInputPayload;
import xyz.nergal.proca.gateway.handler.payload.input.ProductListInputPayload;
import xyz.nergal.proca.gateway.service.ProductService;
import xyz.nergal.proca.product.api.PriceChangeResponse;

import java.time.Duration;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class ProductWebSocketHandler implements WebSocketHandler {

    private static final Duration PING_PERIOD = Duration.ofSeconds(30);

    private final PriceChangedEventEmitter priceChangedEventEmitter;

    private final ObjectMapper objectMapper;

    private final ProductService productService;

    public ProductWebSocketHandler(
            PriceChangedEventEmitter priceChangedEventEmitter,
            ObjectMapper objectMapper,
            ProductService productService) {
        this.priceChangedEventEmitter = priceChangedEventEmitter;
        this.objectMapper = objectMapper;
        this.productService = productService;
    }

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        Collection<PriceSubscription> priceSubscriptions = ConcurrentHashMap.newKeySet();

        Flux<WebSocketMessage> messageTrigger = buildActionTrigger(session, priceSubscriptions);

        Flux<WebSocketMessage> eventTrigger = buildPriceChangedEventTrigger(session, priceSubscriptions);

        Flux<WebSocketMessage> pingTrigger = buildPingTrigger(session);

        return session.send(Flux.merge(messageTrigger, eventTrigger, pingTrigger));
    }

    private Flux<WebSocketMessage> buildPingTrigger(WebSocketSession session) {
        return Flux.interval(PING_PERIOD)
                .map(l -> new OutputPayload<>(ActionName.Internal.PING, new Object()))
                .flatMap(this::serialize)
                .map(session::textMessage);
    }

    private Flux<WebSocketMessage> buildPriceChangedEventTrigger(WebSocketSession session, Collection<PriceSubscription> priceSubscriptions) {
        return priceChangedEventEmitter.create()
                .doOnError(throwable -> log.error("Price changed event emitter error", throwable))
                .doOnNext(priceChangedEvent -> log.info("Received event: {}", priceChangedEvent))
                .map(PriceChangedEvent::getResponse)
                .filter(priceChangeResponse -> priceSubscriptions.stream()
                        .map(PriceSubscription::getProduct)
                        .map(Product::getId)
                        .anyMatch(id -> Objects.equals(id, priceChangeResponse.getProductId())))
                .flatMap(priceChangeResponse -> this.map(priceSubscriptions, priceChangeResponse))
                .map(priceResponse -> new OutputPayload<>(ActionName.Price.CHANGE, priceResponse))
                .doOnNext(outputPayload -> log.info("Created output: {}", outputPayload))
                .flatMap(this::serialize)
                .map(session::textMessage);
    }

    private Flux<WebSocketMessage> buildActionTrigger(WebSocketSession session, Collection<PriceSubscription> priceSubscriptions) {
        return session.receive()
                .flatMap(webSocketMessage -> this.deserializePayload(webSocketMessage)
                        .doOnNext(inputPayload -> log.error("Received message: {}", inputPayload))
                        .flatMap(inputPayload -> this.handle(inputPayload, priceSubscriptions))
                        .onErrorReturn(JsonParseException.class, new OutputPayload<>(ActionName.Internal.ERROR, "invalid JSON")))
                .onErrorContinue((throwable, o) -> log.error("Message handling error", throwable))
                .doOnNext(outputPayload -> log.error("Created output: {}", outputPayload))
                .flatMap(this::serialize)
                .map(session::textMessage);
    }

    private Mono<PriceChange> map(Collection<PriceSubscription> priceSubscriptions, PriceChangeResponse priceChangeResponse) {
        return Flux.fromIterable(priceSubscriptions)
                .filter(subscription -> Objects.equals(subscription.getProduct().getId(), priceChangeResponse.getProductId()))
                .flatMap(subscription -> this.map(subscription, priceChangeResponse))
                .map(price -> PriceChange.builder()
                        .productId(priceChangeResponse.getProductId())
                        .price(price).build())
                .next();
    }

    private Mono<Price> map(PriceSubscription priceSubscriptions, PriceChangeResponse priceChangeResponse) {
        if (priceSubscriptions.isDetailed()) {
            return this.productService.buildDetailedPrice(priceChangeResponse.getProductId(), priceChangeResponse.getPriceResponse());
        }
        return this.productService.buildSimplePrice(priceChangeResponse.getProductId(), priceChangeResponse.getPriceResponse());
    }

    private Mono<OutputPayload<?>> handle(InputPayload<?> payload, Collection<PriceSubscription> priceSubscriptions) {
        if (ActionName.Internal.PONG.equals(payload.getAction())) {
            log.info("Action internal:pong matched for the given payload: {}", payload);
            return handlePong();
        }
        if (ActionName.Product.UNSUBSCRIBE.equals(payload.getAction())) {
            log.info("Action product:unsubscribe matched for the given payload: {}", payload);
            return handleUnsubscribe(priceSubscriptions);
        }
        if (ActionName.Product.LIST.equals(payload.getAction()) && payload instanceof ProductListInputPayload) {
            log.info("Action product:list matched for the given payload: {}", payload);
            ProductListInputPayload productListInputPayload = (ProductListInputPayload) payload;
            return handleList(payload, priceSubscriptions, productListInputPayload);
        }
        if (ActionName.Product.DETAILS.equals(payload.getAction()) && payload instanceof ProductDetailsInputPayload) {
            log.info("Action product:details matched for the given payload: {}", payload);
            ProductDetailsInputPayload productDetailsInputPayload = (ProductDetailsInputPayload) payload;
            return handleDetails(payload, priceSubscriptions, productDetailsInputPayload);
        }

        log.warn("No action handler matched for the given payload: {}", payload);
        return Mono.just(new OutputPayload<>(ActionName.Internal.ERROR, "unknown action is given"));
    }

    private Mono<OutputPayload<?>> handleDetails(InputPayload<?> payload, Collection<PriceSubscription> priceSubscriptions, ProductDetailsInputPayload productDetailsInputPayload) {
        if (productDetailsInputPayload.getData() == null) {
            return Mono.just(new OutputPayload<>(ActionName.Internal.ERROR, "data is missing from payload"));
        }
        return productService.get(productDetailsInputPayload.getData().getId())
                .doOnNext(products -> updateSubscriptions(priceSubscriptions, true, Streamable.of(products)))
                .map(productResponse -> new OutputPayload<>(payload.getAction(), productResponse));
    }

    private Mono<OutputPayload<?>> handleList(InputPayload<?> payload, Collection<PriceSubscription> priceSubscriptions, ProductListInputPayload productListInputPayload) {
        if (productListInputPayload.getData() == null) {
            return Mono.just(new OutputPayload<>(ActionName.Internal.ERROR, "data is missing from payload"));
        }
        return productService.list(productListInputPayload.getData().getPageable())
                .doOnNext(products -> updateSubscriptions(priceSubscriptions, false, products))
                .map(productResponses -> new OutputPayload<>(payload.getAction(), productResponses));
    }

    private Mono<OutputPayload<?>> handleUnsubscribe(Collection<PriceSubscription> priceSubscriptions) {
        priceSubscriptions.clear();
        return Mono.empty();
    }

    private Mono<OutputPayload<?>> handlePong() {
        return Mono.empty();
    }

    private void updateSubscriptions(Collection<PriceSubscription> priceSubscriptions, boolean detailed, Streamable<Product> products) {
        priceSubscriptions.clear();
        products.stream()
                .map(product -> PriceSubscription.builder()
                        .product(product)
                        .detailed(detailed)
                        .build())
                .forEach(priceSubscriptions::add);
    }

    private Mono<InputPayload<?>> deserializePayload(WebSocketMessage webSocketMessage) {
        // Has to access payload as asynchronous operation might close it before
        // objectMapper.readValue is called.
        return Mono.just(webSocketMessage.getPayloadAsText())
                // https://projectreactor.io/docs/core/3.3.1.RELEASE/reference/#faq.wrap-blocking
                .flatMap(s -> Mono.<InputPayload<?>>fromCallable(() -> objectMapper.readValue(s, InputPayload.class))
                        .subscribeOn(Schedulers.boundedElastic()));
    }

    private Mono<String> serialize(OutputPayload<?> object) {
        // https://projectreactor.io/docs/core/3.3.1.RELEASE/reference/#faq.wrap-blocking
        return Mono.fromCallable(() -> objectMapper.writeValueAsString(object))
                .subscribeOn(Schedulers.boundedElastic());
    }
}
