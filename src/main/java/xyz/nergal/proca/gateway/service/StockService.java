package xyz.nergal.proca.gateway.service;

import reactor.core.publisher.Mono;
import xyz.nergal.proca.gateway.data.Stock;
import xyz.nergal.proca.product.api.PriceResponse;

public interface StockService {

    /**
     * Return a stock matching the given product id and the supplier id.
     *
     * @param supplier the supplier id of the expected stock.
     * @param product  the product id of the expected stock.
     * @return a stock matching the given product id and the supplier id.
     */
    Mono<Stock> get(String supplier, Long product);

    default Mono<Stock> get(PriceResponse priceResponse, Long product) {
        return Mono.just(priceResponse)
                .map(PriceResponse::getSupplier)
                .flatMap(supplier -> this.get(supplier, product));
    }
}
