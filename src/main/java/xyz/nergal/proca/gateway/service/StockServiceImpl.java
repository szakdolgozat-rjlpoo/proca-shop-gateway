package xyz.nergal.proca.gateway.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.gateway.configuration.properties.RemoteServiceProperties;
import xyz.nergal.proca.gateway.data.Stock;
import xyz.nergal.proca.supply.api.StockResponse;

import java.time.Duration;

@Slf4j
@Service
public class StockServiceImpl implements StockService {

    private final WebClient webClient;

    public StockServiceImpl(
            WebClient.Builder webClientBuilder,
            RemoteServiceProperties remoteServiceProperties) {
        this.webClient = webClientBuilder
                .baseUrl(remoteServiceProperties.getSupplierHost())
                .build();
    }

    @Override
    public Mono<Stock> get(String supplier, Long product) {
        return this.webClient
                .get()
                .uri(String.format("/suppliers/%s/stocks/%d", supplier, product))
                .retrieve()
                .bodyToMono(StockResponse.class)
                .doOnError(throwable -> log.error("Could not fetch stock", throwable))
                .timeout(Duration.ofSeconds(3))
                .map(this::mapOutput);
    }

    private Stock mapOutput(StockResponse response) {
        return Stock.builder()
                .amount(response.getAmount())
                .build();
    }
}
