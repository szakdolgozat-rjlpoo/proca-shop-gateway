package xyz.nergal.proca.gateway.service;

import reactor.core.publisher.Mono;
import xyz.nergal.proca.gateway.data.Supplier;
import xyz.nergal.proca.product.api.PriceResponse;

public interface SupplierService {

    /**
     * Return a supplier matching the given product id and the supplier id.
     *
     * @param id the id of the expected supplier.
     * @return a supplier matching the given id.
     */
    Mono<Supplier> get(String id);

    default Mono<Supplier> get(PriceResponse priceResponse) {
        return Mono.just(priceResponse)
                .map(PriceResponse::getSupplier)
                .flatMap(this::get);
    }
}
