package xyz.nergal.proca.gateway.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.common.data.PageableQueryParameterBuilder;
import xyz.nergal.proca.gateway.configuration.properties.RemoteServiceProperties;
import xyz.nergal.proca.gateway.data.*;
import xyz.nergal.proca.product.api.PriceResponse;
import xyz.nergal.proca.product.api.ProductResponse;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    private final WebClient webClient;

    private final SupplierService supplierService;

    private final StockService stockService;

    public ProductServiceImpl(
            WebClient.Builder webClientBuilder,
            RemoteServiceProperties remoteServiceProperties,
            SupplierService supplierService,
            StockService stockService) {
        this.supplierService = supplierService;
        this.stockService = stockService;
        this.webClient = webClientBuilder
                .baseUrl(remoteServiceProperties.getProductHost())
                .build();
    }

    @Override
    public Mono<Page<Product>> list(Pageable pageable) {
        PageableQueryParameterBuilder builder = new PageableQueryParameterBuilder();
        return this.webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path("/products")
                        .query(builder.build(pageable))
                        .build())
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<Page<ProductResponse>>() {
                })
                .timeout(Duration.ofSeconds(3))
                .flatMap(this::mapListOutput);
    }

    @Override
    public Mono<Product> get(Long id) {
        return this.webClient
                .get()
                .uri(String.format("/products/%d", id))
                .retrieve()
                .bodyToMono(ProductResponse.class)
                .timeout(Duration.ofSeconds(3))
                .flatMap(productResponse -> Mono.just(productResponse)
                        .flatMapIterable(ProductResponse::getPrices)
                        .flatMap(priceResponse -> this.buildDetailedPrice(productResponse, priceResponse))
                        .collect(Collectors.toList())
                        .map(prices -> this.mapDetailsOutput(productResponse, prices)));
    }

    private Mono<Page<Product>> mapListOutput(Page<ProductResponse> page) {
        return Flux.fromIterable(page)
                .flatMap(productResponse -> Flux.fromIterable(productResponse.getPrices())
                        .flatMap(priceResponses -> this.buildSimplePrice(productResponse, priceResponses))
                        .collectList()
                        .map(prices -> this.map(productResponse, prices)))
                .collectList()
                .map(productResponses -> new PageImpl<>(productResponses, page.getPageable(), page.getTotalElements()));
    }

    private Product map(ProductResponse productResponse, List<Price> prices) {
        return Product.builder()
                .id(productResponse.getId())
                .name(productResponse.getName())
                .description(productResponse.getDescription())
                .img(productResponse.getImg())
                .categories(productResponse.getCategories())
                .prices(prices)
                .build();
    }

    private Mono<Price> buildSimplePrice(ProductResponse productResponse, PriceResponse priceResponse) {
        return this.buildSimplePrice(productResponse.getId(), priceResponse);
    }

    public Mono<Price> buildSimplePrice(Long productId, PriceResponse priceResponse) {
        return stockService.get(priceResponse, productId)
                .map(stock -> this.buildSimplePrice(priceResponse, stock));
    }

    private Price buildSimplePrice(PriceResponse response, Stock stock) {
        return Price.builder()
                .id(response.getId())
                .currencyCode(response.getCurrencyCode())
                .value(response.getValue())
                .detail(PriceDetail.builder()
                        .stock(stock.getAmount())
                        .build())
                .updatedAt(response.getUpdatedAt())
                .build();
    }

    private Product mapDetailsOutput(ProductResponse productResponse, List<Price> prices) {
        return Product.builder()
                .id(productResponse.getId())
                .name(productResponse.getName())
                .description(productResponse.getDescription())
                .img(productResponse.getImg())
                .categories(productResponse.getCategories())
                .prices(prices)
                .build();
    }

    private Mono<Price> buildDetailedPrice(ProductResponse productResponse, PriceResponse priceResponse) {
        return this.buildDetailedPrice(productResponse.getId(), priceResponse);
    }

    public Mono<Price> buildDetailedPrice(Long productId, PriceResponse priceResponse) {
        return Mono.zip(supplierService.get(priceResponse), stockService.get(priceResponse, productId))
                .map(tuple -> this.buildDetailedPrice(priceResponse, tuple.getT1(), tuple.getT2()));
    }

    private Price buildDetailedPrice(PriceResponse response, Supplier supplier, Stock stock) {
        return Price.builder()
                .id(response.getId())
                .currencyCode(response.getCurrencyCode())
                .value(response.getValue())
                .detail(PriceDetail.builder()
                        .supplier(supplier.getName())
                        .stock(stock.getAmount())
                        .build())
                .updatedAt(response.getUpdatedAt())
                .build();
    }
}
