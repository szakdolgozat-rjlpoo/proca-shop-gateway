package xyz.nergal.proca.gateway.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.gateway.data.Price;
import xyz.nergal.proca.gateway.data.Product;
import xyz.nergal.proca.product.api.PriceResponse;

public interface ProductService {

    /**
     * Return a list of product for the given page.
     *
     * @param pageable the pageable information.
     * @return list of product.
     */
    Mono<Page<Product>> list(Pageable pageable);

    /**
     * Return a product matching the given id.
     *
     * @param id the id of the expected product.
     * @return a product matching the given id.
     */
    Mono<Product> get(Long id);

    /**
     * Return a simple price matching the given product id and the supplier of the giver price response.
     *
     * A simple price has **no** information about its supplier.
     *
     * @param productId     the product id of the expected price.
     * @param priceResponse the price response which contains the supplier.
     * @return a price matching the given product id and the supplier of the giver price response.
     */
    Mono<Price> buildSimplePrice(Long productId, PriceResponse priceResponse);

    /**
     * Return a detailed price matching the given product id and the supplier of the giver price response.
     *
     * A detailed price has information about its supplier.
     *
     * @param productId     the product id of the expected price.
     * @param priceResponse the price response which contains the supplier.
     * @return a price matching the given product id and the supplier of the giver price response.
     */
    Mono<Price> buildDetailedPrice(Long productId, PriceResponse priceResponse);
}
