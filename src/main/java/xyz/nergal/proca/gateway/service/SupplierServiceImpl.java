package xyz.nergal.proca.gateway.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import xyz.nergal.proca.gateway.configuration.properties.RemoteServiceProperties;
import xyz.nergal.proca.gateway.data.Supplier;
import xyz.nergal.proca.supply.api.SupplierResponse;

import java.time.Duration;

@Slf4j
@Service
public class SupplierServiceImpl implements SupplierService {

    private final WebClient webClient;

    public SupplierServiceImpl(
            WebClient.Builder webClientBuilder,
            RemoteServiceProperties remoteServiceProperties) {
        this.webClient = webClientBuilder
                .baseUrl(remoteServiceProperties.getSupplierHost())
                .build();
    }

    @Override
    public Mono<Supplier> get(String id) {
        return this.webClient
                .get()
                .uri(String.format("/suppliers/%s", id))
                .retrieve()
                .bodyToMono(SupplierResponse.class)
                .doOnError(throwable -> log.error("Could not fetch supplier", throwable))
                .timeout(Duration.ofSeconds(3))
                .map(this::mapOutput);
    }

    private Supplier mapOutput(SupplierResponse response) {
        return Supplier.builder()
                .name(response.getName())
                .build();
    }
}
