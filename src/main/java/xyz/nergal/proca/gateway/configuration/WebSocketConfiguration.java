package xyz.nergal.proca.gateway.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import xyz.nergal.proca.gateway.handler.ProductWebSocketHandler;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class WebSocketConfiguration {

    private static final int ORDER_BEFORE_ANNOTATED_CONTROLLERS = -1;

    private final ProductWebSocketHandler productWebSocketHandler;

    public WebSocketConfiguration(ProductWebSocketHandler productWebSocketHandler) {
        this.productWebSocketHandler = productWebSocketHandler;
    }

    @Bean
    public HandlerMapping handlerMapping() {
        Map<String, WebSocketHandler> map = new HashMap<>();
        map.put("/ws", productWebSocketHandler);
        return new SimpleUrlHandlerMapping(map, ORDER_BEFORE_ANNOTATED_CONTROLLERS);
    }

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }
}
