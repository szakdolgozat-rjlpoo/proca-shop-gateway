package xyz.nergal.proca.gateway.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import xyz.nergal.proca.product.api.PriceChangeResponse;

@Configuration
public class RedisReactiveConfiguration {

    @Bean
    public ReactiveRedisTemplate<String, PriceChangeResponse> reactiveRedisTemplate(
            ReactiveRedisConnectionFactory reactiveRedisConnectionFactory,
            ObjectMapper objectMapper) {
        StringRedisSerializer keySerializer = new StringRedisSerializer();
        Jackson2JsonRedisSerializer<PriceChangeResponse> valueSerializer = new Jackson2JsonRedisSerializer<>(PriceChangeResponse.class);
        valueSerializer.setObjectMapper(objectMapper);
        RedisSerializationContext.RedisSerializationContextBuilder<String, PriceChangeResponse> builder = RedisSerializationContext.newSerializationContext(keySerializer);
        RedisSerializationContext<String, PriceChangeResponse> serializationContext = builder.value(valueSerializer).build();
        return new ReactiveRedisTemplate<>(reactiveRedisConnectionFactory, serializationContext);
    }
}
