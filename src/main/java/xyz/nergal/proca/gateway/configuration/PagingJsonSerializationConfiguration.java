package xyz.nergal.proca.gateway.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import xyz.nergal.proca.common.databind.PageJsonComponent;
import xyz.nergal.proca.common.databind.PageableJsonDeserializer;
import xyz.nergal.proca.common.databind.SortJsonComponent;

@Configuration
public class PagingJsonSerializationConfiguration {

    @Bean
    public PageJsonComponent pageJsonComponent() {
        return new PageJsonComponent();
    }

    @Bean
    public SortJsonComponent sortJsonComponent() {
        return new SortJsonComponent();
    }

    @Bean
    public PageableJsonDeserializer pageableJsonDeserializer() {
        return new PageableJsonDeserializer();
    }
}
