package xyz.nergal.proca.gateway.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.ManagedBean;

@ManagedBean
@ConfigurationProperties(prefix = "app.remote")
public class RemoteServiceProperties {

    private String productHost;

    private String supplierHost;

    public String getProductHost() {
        return productHost;
    }

    public void setProductHost(String productHost) {
        this.productHost = productHost;
    }

    public String getSupplierHost() {
        return supplierHost;
    }

    public void setSupplierHost(String supplierHost) {
        this.supplierHost = supplierHost;
    }
}
