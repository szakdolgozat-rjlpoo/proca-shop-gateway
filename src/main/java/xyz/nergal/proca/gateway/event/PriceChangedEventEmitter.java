package xyz.nergal.proca.gateway.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.ReactiveSubscription;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import xyz.nergal.proca.product.api.PriceChangeResponse;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class PriceChangedEventEmitter {

    private final ReactiveRedisTemplate<String, PriceChangeResponse> reactiveRedisTemplate;

    private Flux<PriceChangedEvent> eventFlux;

    public PriceChangedEventEmitter(ReactiveRedisTemplate<String, PriceChangeResponse> reactiveRedisTemplate) {
        this.reactiveRedisTemplate = reactiveRedisTemplate;
    }

    @PostConstruct
    public void onPostConstruct() {
        eventFlux = Flux.from(reactiveRedisTemplate.listenToChannel("price-change")
                .map(ReactiveSubscription.Message::getMessage)
                .map(PriceChangedEvent::new)).publish().autoConnect();
    }

    public Flux<PriceChangedEvent> create() {
        return Flux.from(eventFlux);
    }
}
