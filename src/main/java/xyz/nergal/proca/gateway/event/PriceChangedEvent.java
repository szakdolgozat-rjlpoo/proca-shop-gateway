package xyz.nergal.proca.gateway.event;

import lombok.EqualsAndHashCode;
import xyz.nergal.proca.product.api.PriceChangeResponse;

@EqualsAndHashCode
public class PriceChangedEvent {

    private final PriceChangeResponse response;

    public PriceChangedEvent(PriceChangeResponse response) {
        this.response = response;
    }

    public PriceChangeResponse getResponse() {
        return response;
    }
}
