package xyz.nergal.proca.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import xyz.nergal.proca.gateway.configuration.properties.RemoteServiceProperties;

@SpringBootApplication
@EnableConfigurationProperties(RemoteServiceProperties.class)
public class ApiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayApplication.class, args);
    }

}
