package xyz.nergal.proca.gateway.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import xyz.nergal.proca.common.io.ResourceUtils;
import xyz.nergal.proca.gateway.configuration.PagingJsonSerializationConfiguration;
import xyz.nergal.proca.gateway.configuration.properties.RemoteServiceProperties;
import xyz.nergal.proca.gateway.data.Price;
import xyz.nergal.proca.gateway.data.PriceDetail;
import xyz.nergal.proca.gateway.data.Stock;
import xyz.nergal.proca.gateway.data.Supplier;
import xyz.nergal.proca.product.api.PriceResponse;

import java.io.IOException;

@ExtendWith(SpringExtension.class)
@Import({
        PagingJsonSerializationConfiguration.class,
        WebClientAutoConfiguration.class,
        JacksonAutoConfiguration.class,
})
class ProductServiceImplTest {

    private ProductService productService;

    private MockWebServer mockWebServer;

    @Mock
    private SupplierService supplierService;

    @Mock
    private StockService stockService;

    @Autowired
    private WebClient.Builder webClientBuilder;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        this.mockWebServer = new MockWebServer();

        RemoteServiceProperties remoteServiceProperties = new RemoteServiceProperties();
        remoteServiceProperties.setProductHost(this.mockWebServer.url("/").toString());

        ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
                .codecs(clientCodecConfigurer -> {
                    clientCodecConfigurer.defaultCodecs().jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper, MediaType.APPLICATION_JSON));
                    clientCodecConfigurer.defaultCodecs().jackson2JsonEncoder(new Jackson2JsonEncoder(objectMapper, MediaType.APPLICATION_JSON));
                })
                .build();

        this.productService = new ProductServiceImpl(
                webClientBuilder.exchangeStrategies(exchangeStrategies),
                remoteServiceProperties,
                supplierService, stockService);
    }

    @AfterEach
    void tearDown() throws IOException {
        this.mockWebServer.shutdown();
    }

    @Test
    void list() throws IOException {
        Mockito.when(stockService.get(ArgumentMatchers.any(PriceResponse.class), ArgumentMatchers.any(Long.class)))
                .thenReturn(Mono.just(Stock.builder().build()));

        String responseBody = ResourceUtils.asString(new ClassPathResource("static/third-party/product-index-payload.json"));

        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setBody(responseBody));

        StepVerifier.create(productService.list(Pageable.unpaged()))
                .consumeNextWith(productResponses -> {
                    Assertions.assertNotNull(productResponses);
                    Assertions.assertTrue(productResponses.hasContent());
                    Assertions.assertEquals(1, productResponses.getNumberOfElements());
                    Assertions.assertEquals(20, productResponses.getSize());
                    Assertions.assertEquals("name", productResponses.getContent().get(0).getName());
                })
                .verifyComplete();
    }

    @Test
    void get() throws IOException {
        Mockito.when(supplierService.get(ArgumentMatchers.any(PriceResponse.class)))
                .thenReturn(Mono.just(Supplier.builder().build()));
        Mockito.when(stockService.get(ArgumentMatchers.any(PriceResponse.class), ArgumentMatchers.any(Long.class)))
                .thenReturn(Mono.just(Stock.builder().build()));

        String responseBody = ResourceUtils.asString(new ClassPathResource("static/third-party/product-read-payload.json"));

        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setBody(responseBody));

        StepVerifier.create(productService.get(1L))
                .consumeNextWith(product -> {
                    Assertions.assertNotNull(product);
                    Assertions.assertEquals(1L, product.getId());
                })
                .verifyComplete();
    }

    @Test
    void buildSimplePrice() {
        Mockito.when(stockService.get(ArgumentMatchers.any(PriceResponse.class), ArgumentMatchers.any(Long.class)))
                .thenReturn(Mono.just(Stock.builder().amount(1L).build()));


        StepVerifier.create(productService.buildSimplePrice(1L, PriceResponse.builder().build()))
                .assertNext(actual -> {
                    PriceDetail expectedDetail = PriceDetail.builder()
                            .stock(1L)
                            .build();
                    Price expected = Price.builder().detail(expectedDetail).build();
                    Assertions.assertEquals(expected, actual);
                })
                .verifyComplete();
    }

    @Test
    void buildDetailedPrice() {
        Mockito.when(supplierService.get(ArgumentMatchers.any(PriceResponse.class)))
                .thenReturn(Mono.just(Supplier.builder().name("name").build()));
        Mockito.when(stockService.get(ArgumentMatchers.any(PriceResponse.class), ArgumentMatchers.any(Long.class)))
                .thenReturn(Mono.just(Stock.builder().amount(1L).build()));

        StepVerifier.create(productService.buildDetailedPrice(1L, PriceResponse.builder().build()))
                .assertNext(actual -> {
                    PriceDetail expectedDetail = PriceDetail.builder()
                            .supplier("name")
                            .stock(1L)
                            .build();
                    Price expected = Price.builder().detail(expectedDetail).build();
                    Assertions.assertEquals(expected, actual);
                })
                .verifyComplete();
    }
}
