package xyz.nergal.proca.gateway.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.test.StepVerifier;
import xyz.nergal.proca.common.io.ResourceUtils;
import xyz.nergal.proca.gateway.configuration.PagingJsonSerializationConfiguration;
import xyz.nergal.proca.gateway.configuration.properties.RemoteServiceProperties;

import java.io.IOException;

@ExtendWith(SpringExtension.class)
@Import({
        PagingJsonSerializationConfiguration.class,
        WebClientAutoConfiguration.class,
        JacksonAutoConfiguration.class,
})
class StockServiceImplTest {

    private StockService stockService;

    private MockWebServer mockWebServer;

    @Autowired
    private WebClient.Builder webClientBuilder;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        this.mockWebServer = new MockWebServer();

        RemoteServiceProperties remoteServiceProperties = new RemoteServiceProperties();
        remoteServiceProperties.setSupplierHost(this.mockWebServer.url("/").toString());

        ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
                .codecs(clientCodecConfigurer -> {
                    clientCodecConfigurer.defaultCodecs().jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper, MediaType.APPLICATION_JSON));
                    clientCodecConfigurer.defaultCodecs().jackson2JsonEncoder(new Jackson2JsonEncoder(objectMapper, MediaType.APPLICATION_JSON));
                })
                .build();

        this.stockService = new StockServiceImpl(
                webClientBuilder.exchangeStrategies(exchangeStrategies),
                remoteServiceProperties);
    }

    @AfterEach
    void tearDown() throws IOException {
        this.mockWebServer.shutdown();
    }

    @Test
    void get() throws IOException {
        String responseBody = ResourceUtils.asString(new ClassPathResource("static/third-party/stock-read-payload.json"));

        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setBody(responseBody));

        StepVerifier.create(stockService.get("object-id", 1L))
                .consumeNextWith(stock -> {
                    Assertions.assertNotNull(stock);
                    Assertions.assertEquals(1L, stock.getAmount());
                })
                .verifyComplete();
    }
}
