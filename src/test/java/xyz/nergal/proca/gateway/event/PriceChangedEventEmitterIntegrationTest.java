package xyz.nergal.proca.gateway.event;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.test.StepVerifier;
import xyz.nergal.proca.product.api.PriceChangeResponse;
import xyz.nergal.proca.product.api.PriceResponse;

import java.time.Duration;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class PriceChangedEventEmitterIntegrationTest {

    @Autowired
    private PriceChangedEventEmitter priceChangedEventEmitter;

    @Autowired
    private ReactiveRedisTemplate<String, PriceChangeResponse> reactiveRedisTemplate;

    @Test
    void createShouldInvokeNextWhenWhenMessageReceivedFromRedis() {
        StepVerifier.create(priceChangedEventEmitter.create()
                .map(PriceChangedEvent::getResponse)
                .timeout(Duration.ofSeconds(3)))
                .thenAwait(Duration.ofMillis(500))
                .then(() -> {
                    PriceChangeResponse priceChangeResponse = PriceChangeResponse.builder().productId(1L).build();
                    reactiveRedisTemplate.convertAndSend("price-change", priceChangeResponse).subscribe();
                })
                .expectNext(PriceChangeResponse.builder().productId(1L).build())
                .thenCancel()
                .verify();
    }
}
