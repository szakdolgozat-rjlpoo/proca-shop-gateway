package xyz.nergal.proca.gateway.handler.payload;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xyz.nergal.proca.gateway.configuration.PagingJsonSerializationConfiguration;
import xyz.nergal.proca.gateway.handler.payload.input.EmptyInputPayload;
import xyz.nergal.proca.gateway.handler.payload.input.ProductDetailsInputPayload;
import xyz.nergal.proca.gateway.handler.payload.input.ProductListInputPayload;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@Import({
        PagingJsonSerializationConfiguration.class,
        JacksonAutoConfiguration.class,
})
class InputPayloadTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void deserialization() throws JsonProcessingException {
        String stringPayload;
        InputPayload objectPayload;

        stringPayload = "{\"action\":\"any\"}";
        objectPayload = objectMapper.readValue(stringPayload, InputPayload.class);
        Assertions.assertEquals("any", objectPayload.getAction());
        Assertions.assertTrue(objectPayload instanceof EmptyInputPayload);

        stringPayload = "{\"action\":\"product:list\"}";
        objectPayload = objectMapper.readValue(stringPayload, InputPayload.class);
        Assertions.assertEquals("product:list", objectPayload.getAction());
        Assertions.assertTrue(objectPayload instanceof ProductListInputPayload);

        stringPayload = "{\"action\":\"product:details\"}";
        objectPayload = objectMapper.readValue(stringPayload, InputPayload.class);
        Assertions.assertEquals("product:details", objectPayload.getAction());
        Assertions.assertTrue(objectPayload instanceof ProductDetailsInputPayload);
    }
}