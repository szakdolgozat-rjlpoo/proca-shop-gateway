package xyz.nergal.proca.gateway.handler;

import org.json.JSONException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.ReplayProcessor;
import reactor.test.StepVerifier;
import xyz.nergal.proca.common.io.ResourceUtils;
import xyz.nergal.proca.product.api.PriceChangeResponse;
import xyz.nergal.proca.product.api.PriceResponse;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductWebSocketHandlerIntegrationTest {

    private static ClientAndServer mockProductServer;
    private static ClientAndServer mockSupplyServer;

    @LocalServerPort
    private int localServerPort;

    @Autowired
    private ReactiveRedisTemplate<String, PriceChangeResponse> reactiveRedisTemplate;

    @BeforeAll
    static void setUpBeforeClass() throws IOException {
        mockProductServer = ClientAndServer.startClientAndServer(9999);

        String productIndexBody = ResourceUtils.asString(new ClassPathResource("static/third-party/product-index-payload.json"));
        mockProductServer.when(HttpRequest.request("/products").withMethod(HttpMethod.GET.name()))
                .respond(HttpResponse.response(productIndexBody)
                        .withStatusCode(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE));

        String productReadBody = ResourceUtils.asString(new ClassPathResource("static/third-party/product-read-payload.json"));
        mockProductServer.when(HttpRequest.request("/products/1").withMethod(HttpMethod.GET.name()))
                .respond(HttpResponse.response(productReadBody)
                        .withStatusCode(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE));

        mockSupplyServer = ClientAndServer.startClientAndServer(9998);

        String supplyReadBody = ResourceUtils.asString(new ClassPathResource("static/third-party/supplier-read-payload.json"));
        mockSupplyServer.when(HttpRequest.request("/suppliers/object-id").withMethod(HttpMethod.GET.name()))
                .respond(HttpResponse.response(supplyReadBody)
                        .withStatusCode(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE));

        String stockReadBody = ResourceUtils.asString(new ClassPathResource("static/third-party/stock-read-payload.json"));
        mockSupplyServer.when(HttpRequest.request("/suppliers/object-id/stocks/1").withMethod(HttpMethod.GET.name()))
                .respond(HttpResponse.response(stockReadBody)
                        .withStatusCode(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE));
    }

    @AfterAll
    static void tearDownAfterClass() {
        mockProductServer.stop();
        mockSupplyServer.stop();
    }

    @Test
    void handlerShouldSendErrorWhenActionCanNotBeHandled() throws URISyntaxException {
        WebSocketClient client = new ReactorNettyWebSocketClient();
        ReplayProcessor<WebSocketMessage> output = ReplayProcessor.create();

        URI url = new URI("ws://localhost:" + localServerPort + "/ws");

        String payload = "{\"action\":\"any\"}";
        client.execute(url, session -> Flux.merge(
                session.send(Flux.just(session.textMessage(payload))),
                session.receive()
                        .subscribeWith(output)
                        .doOnNext(webSocketMessage -> session.close().subscribe())
                        .then()
        ).timeout(Duration.ofSeconds(3)).then()).doOnError(output::onError).subscribe();

        String expected = "{\"action\":\"internal:error\",\"data\":\"unknown action is given\"}";
        StepVerifier.create(output)
                .consumeNextWith(message -> {
                    try {
                        String payloadAsText = message.getPayloadAsText();
                        JSONAssert.assertEquals(expected, payloadAsText, false);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                })
                .verifyComplete();
    }

    @Test
    void handlerShouldSendErrorWhenPayloadContainsInvalidJson() throws URISyntaxException {
        WebSocketClient client = new ReactorNettyWebSocketClient();
        ReplayProcessor<WebSocketMessage> output = ReplayProcessor.create();

        URI url = new URI("ws://localhost:" + localServerPort + "/ws");

        String payload = "any";
        client.execute(url, session -> Flux.merge(
                session.send(Flux.just(session.textMessage(payload))),
                session.receive()
                        .subscribeWith(output)
                        .doOnNext(webSocketMessage -> session.close().subscribe())
                        .then()
        ).timeout(Duration.ofSeconds(3)).then()).doOnError(output::onError).subscribe();

        String expected = "{\"action\":\"internal:error\",\"data\":\"invalid JSON\"}";
        StepVerifier.create(output)
                .consumeNextWith(message -> {
                    try {
                        String payloadAsText = message.getPayloadAsText();
                        JSONAssert.assertEquals(expected, payloadAsText, false);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                })
                .verifyComplete();
    }

    @Test
    void handlerShouldSendErrorWhenProductListDataIsEmpty() throws URISyntaxException {
        WebSocketClient client = new ReactorNettyWebSocketClient();
        ReplayProcessor<WebSocketMessage> output = ReplayProcessor.create();

        URI url = new URI("ws://localhost:" + localServerPort + "/ws");

        String payload = "{\"action\":\"product:list\"}";
        client.execute(url, session -> Flux.merge(
                session.send(Flux.just(session.textMessage(payload))),
                session.receive()
                        .subscribeWith(output)
                        .doOnNext(webSocketMessage -> session.close().subscribe())
                        .then()
        ).timeout(Duration.ofSeconds(3)).then()).doOnError(output::onError).subscribe();

        String expected = "{\"action\":\"internal:error\",\"data\":\"data is missing from payload\"}";
        StepVerifier.create(output)
                .consumeNextWith(message -> {
                    try {
                        String payloadAsText = message.getPayloadAsText();
                        JSONAssert.assertEquals(expected, payloadAsText, false);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                })
                .verifyComplete();
    }

    @Test
    void handlerShouldSendProductList() throws URISyntaxException, IOException {
        WebSocketClient client = new ReactorNettyWebSocketClient();
        ReplayProcessor<WebSocketMessage> output = ReplayProcessor.create();

        URI url = new URI("ws://localhost:" + localServerPort + "/ws");

        String data = "{}";
        String payload = "{\"action\":\"product:list\",\"data\":" + data + "}";
        client.execute(url, session -> Flux.merge(
                session.send(Flux.just(session.textMessage(payload))),
                session.receive()
                        .subscribeWith(output)
                        .doOnNext(webSocketMessage -> session.close().subscribe())
                        .then()
        ).timeout(Duration.ofSeconds(3)).then()).doOnError(output::onError).subscribe();

        String expected = ResourceUtils.asString(new ClassPathResource("static/product-list-output.json"));
        StepVerifier.create(output)
                .consumeNextWith(message -> {
                    try {
                        String payloadAsText = message.getPayloadAsText();
                        JSONAssert.assertEquals(expected, payloadAsText, false);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                })
                .verifyComplete();
    }

    @Test
    void handlerShouldSendErrorWhenProductDetailsDataIsEmpty() throws URISyntaxException {
        WebSocketClient client = new ReactorNettyWebSocketClient();
        ReplayProcessor<WebSocketMessage> output = ReplayProcessor.create();

        URI url = new URI("ws://localhost:" + localServerPort + "/ws");

        String payload = "{\"action\":\"product:details\"}";
        client.execute(url, session -> Flux.merge(
                session.send(Flux.just(session.textMessage(payload))),
                session.receive()
                        .subscribeWith(output)
                        .doOnNext(webSocketMessage -> session.close().subscribe())
                        .then()
        ).timeout(Duration.ofSeconds(3)).then()).doOnError(output::onError).subscribe();

        String expected = "{\"action\":\"internal:error\",\"data\":\"data is missing from payload\"}";
        StepVerifier.create(output)
                .consumeNextWith(message -> {
                    try {
                        JSONAssert.assertEquals(expected, message.getPayloadAsText(), false);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                })
                .verifyComplete();
    }

    @Test
    void handlerShouldSendProductDetails() throws URISyntaxException, IOException {
        WebSocketClient client = new ReactorNettyWebSocketClient();
        ReplayProcessor<WebSocketMessage> output = ReplayProcessor.create();

        URI url = new URI("ws://localhost:" + localServerPort + "/ws");

        String data = "{\"id\":1}";
        String payload = "{\"action\":\"product:details\",\"data\":" + data + "}";
        client.execute(url, session -> Flux.merge(
                session.send(Flux.just(session.textMessage(payload))),
                session.receive()
                        .subscribeWith(output)
                        .doOnNext(webSocketMessage -> session.close().subscribe())
                        .then()
        ).timeout(Duration.ofSeconds(3)).then()).doOnError(output::onError).subscribe();

        String expected = ResourceUtils.asString(new ClassPathResource("static/product-details-output.json"));
        StepVerifier.create(output)
                .consumeNextWith(message -> {
                    try {
                        JSONAssert.assertEquals(expected, message.getPayloadAsText(), false);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                })
                .verifyComplete();
    }

    @Test
    void handlerShouldSendPriceChangeWhenSubscribedToProductList() throws URISyntaxException {
        WebSocketClient client = new ReactorNettyWebSocketClient();
        ReplayProcessor<WebSocketMessage> output = ReplayProcessor.create();

        URI url = new URI("ws://localhost:" + localServerPort + "/ws");

        String data = "{\"pageable\":{\"page\":1,\"size\":20}}";
        String payload = "{\"action\":\"product:list\",\"data\":" + data + "}";
        client.execute(url, session -> Flux.merge(
                session.send(Flux.just(session.textMessage(payload))),
                session.receive()
                        .subscribeWith(output)
                        .buffer(2)
                        .doOnNext(webSocketMessages -> session.close().subscribe())
                        .then()
        ).timeout(Duration.ofSeconds(3)).then()).doOnError(output::onError).subscribe();

        StepVerifier.create(output)
                .thenAwait(Duration.ofMillis(500))
                .then(() -> {
                    PriceChangeResponse priceChangeResponse = PriceChangeResponse.builder()
                            .productId(1L)
                            .priceResponse(PriceResponse.builder().id(1L).supplier("object-id").build())
                            .build();
                    reactiveRedisTemplate.convertAndSend("price-change", priceChangeResponse).subscribe();
                })
                .thenAwait(Duration.ofMillis(10))
                .expectNextCount(1)
                .assertNext(message -> {
                    try {
                        String expectedDetail = "\"detail\":{\"supplier\":null,\"stock\": 1}";
                        String expectedPrice = "\"price\":{\"id\":1,\"currencyCode\":null,\"value\":null," + expectedDetail + ",\"updatedAt\":null}";
                        String expected = "{\"action\":\"price:change\",\"data\":{\"productId\":1," + expectedPrice + "}}";
                        JSONAssert.assertEquals(expected, message.getPayloadAsText(), true);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                })
                .verifyComplete();
    }

    @Test
    void handlerShouldSendPriceChangeWhenSubscribedToProductListDetails() throws URISyntaxException {
        WebSocketClient client = new ReactorNettyWebSocketClient();
        ReplayProcessor<WebSocketMessage> output = ReplayProcessor.create();

        URI url = new URI("ws://localhost:" + localServerPort + "/ws");

        String data = "{\"id\":1}";
        String payload = "{\"action\":\"product:details\",\"data\":" + data + "}";
        client.execute(url, session -> Flux.merge(
                session.send(Flux.just(session.textMessage(payload))),
                session.receive()
                        .subscribeWith(output)
                        .buffer(2)
                        .doOnNext(webSocketMessages -> session.close().subscribe())
                        .then()
        ).timeout(Duration.ofSeconds(3)).then()).doOnError(output::onError).subscribe();

        StepVerifier.create(output)
                .thenAwait(Duration.ofMillis(500))
                .then(() -> {
                    PriceChangeResponse priceChangeResponse = PriceChangeResponse.builder()
                            .productId(1L)
                            .priceResponse(PriceResponse.builder().id(1L).supplier("object-id").build())
                            .build();
                    reactiveRedisTemplate.convertAndSend("price-change", priceChangeResponse).subscribe();
                })
                .thenAwait(Duration.ofMillis(10))
                .expectNextCount(1)
                .assertNext(message -> {
                    try {
                        String expectedDetail = "\"detail\":{\"supplier\":\"name\",\"stock\": 1}";
                        String expectedPrice = "\"price\":{\"id\":1,\"currencyCode\":null,\"value\":null," + expectedDetail + ",\"updatedAt\":null}";
                        String expected = "{\"action\":\"price:change\",\"data\":{\"productId\":1," + expectedPrice + "}}";
                        JSONAssert.assertEquals(expected, message.getPayloadAsText(), true);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                })
                .verifyComplete();
    }

    @Test
    void handlerShouldPingClient() throws URISyntaxException {
        WebSocketClient client = new ReactorNettyWebSocketClient();
        ReplayProcessor<WebSocketMessage> output = ReplayProcessor.create();

        URI url = new URI("ws://localhost:" + localServerPort + "/ws");

        client.execute(url, session -> Flux.merge(
                session.receive()
                        .subscribeWith(output)
                        .doOnNext(webSocketMessage -> session.close().subscribe())
                        .then()
        ).timeout(Duration.ofSeconds(31)).then()).doOnError(output::onError).subscribe();

        StepVerifier.create(output)
                .thenAwait(Duration.ofMillis(500))
                .assertNext(message -> {
                    try {
                        String expected = "{\"action\":\"internal:ping\",\"data\":{}}";
                        JSONAssert.assertEquals(expected, message.getPayloadAsText(), true);
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                })
                .verifyComplete();
    }
}
