FROM gradle:jdk8 as build

COPY . /workspace/app
WORKDIR /workspace/app
RUN gradle build -x test --no-daemon

FROM adoptopenjdk/openjdk8

EXPOSE 8130

RUN mkdir /app

COPY --from=build /workspace/app/build/libs/*.jar /app/spring-boot-application.jar

ENTRYPOINT [ "java", "-Dspring.profiles.active=prod", "-jar", "/app/spring-boot-application.jar" ]
