# API Gateway

[![pipeline status](https://gitlab.com/szakdolgozat-rjlpoo/proca-shop-gateway/badges/master/pipeline.svg)](https://gitlab.com/szakdolgozat-rjlpoo/proca-shop-gateway/commits/master)
[![coverage report](https://gitlab.com/szakdolgozat-rjlpoo/proca-shop-gateway/badges/master/coverage.svg)](https://gitlab.com/szakdolgozat-rjlpoo/proca-shop-gateway/commits/master)

## Endpoints

### WebSocket endpoints

```
GET /ws
Permit all
```

#### WebSocket events

`product:list`

Input

`$pageable.size` must be greater than zero

```json
{
  "action": "product:list",
  "data": {
      "pageable": {
        "page": 0,
        "size": 1
      }
  }
}
```

Output

`$.data.content[].prices[].detail.supplier` is always null

```json
{
  "action": "product:list",
  "data": {
    "content": [
      {
        "id": 1,
        "name": "",
        "description": "",
        "img": "",
        "categories": [
          ""
        ],
        "prices": [
          {
            "id": 1,
            "currencyCode": "",
            "value": 1.00,
            "updatedAt": "1970-01-01T00:00:01",
            "detail": {
              "supplier": null,
              "stock": 1
            }
          }
        ]
      }
    ],
    "sort": [],
    "totalElements": 1,
    "size": 1,
    "number": 0
  }
}
```

Continuous output

`$.data.detail.supplier` is always null

```json
{
  "action": "price:change",
  "data": {
    "productId": 1,
    "price": {
      "id": 1,
      "currencyCode": "",
      "value": 1.00,
      "updatedAt": "1970-01-01T00:00:01",
      "detail": {
        "supplier": null,
        "stock": 1
      }
    }
  }
}
```

`product:details`

Input

```json
{
  "action": "product:details",
  "data": {
    "id": 1
  }
}
```

Output

```json
{
  "action": "product:details",
  "data": {
    "id": 1,
    "name": "",
    "description": "",
    "img": "",
    "categories": [
      ""
    ],
    "prices": [
      {
        "id": 1,
        "currencyCode": "",
        "value": 1.00,
        "updatedAt": "1970-01-01T00:00:01",
        "detail": {
          "supplier": "",
          "stock": 1
        }
      }
    ]
  }
}
```

Continuous output

```json
{
  "action": "price:change",
  "data": {
    "productId": 1,
    "price": {
      "id": 1,
      "currencyCode": "",
      "value": 1.00,
      "updatedAt": "1970-01-01T00:00:01",
      "detail": {
        "supplier": "",
        "stock": 1
      }
    }
  }
}
```

`product:unsubscribe`

Input

```json
{
  "action": "product:unsubscribe"
}
```

No output


Stops continuous output
